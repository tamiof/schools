package com.example.wt

import com.example.wt.model.SchoolModel
import com.example.wt.repository.SchoolsRepository
import com.example.wt.viewmodel.SchoolRequestState
import com.example.wt.viewmodel.SchoolsViewModel
import com.google.common.truth.Truth.assertThat
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class SchoolsViewModelTest {
    companion object {
        private const val SCHOOL_ID = "123"
    }

    @MockK
    private lateinit var schoolsViewModel: SchoolsViewModel

    @MockK
    private lateinit var schoolsRepository: SchoolsRepository

    @MockK
    private lateinit var school1: SchoolModel

    @MockK
    private lateinit var school2: SchoolModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(TestCoroutineDispatcher())
    }

    @After
    fun tearDown() {
        clearAllMocks()
        Dispatchers.resetMain()
    }

    @Test
    fun `getSchool success`() = runTest {
        coEvery { schoolsRepository.getSchools() } returns listOf()
        coEvery { schoolsRepository.getSchool(SCHOOL_ID) } returns school1

        schoolsViewModel = SchoolsViewModel(schoolsRepository)

        schoolsViewModel.getSchool(SCHOOL_ID)
        assertThat(schoolsViewModel.schoolState.value).isEqualTo(SchoolRequestState.Success(school1))
    }

    @Test
    fun `getSchool failure different school than specified id`() = runTest {
        coEvery { schoolsRepository.getSchools() } returns listOf()
        coEvery { schoolsRepository.getSchool(SCHOOL_ID) } returns school1

        schoolsViewModel = SchoolsViewModel(schoolsRepository)

        schoolsViewModel.getSchool(SCHOOL_ID)
        assertThat(schoolsViewModel.schoolState.value).isNotEqualTo(SchoolRequestState.Success(school2))
    }

    @Test
    fun `destinationSchoolRoute success`() = runTest {
        coEvery { schoolsRepository.getSchools() } returns listOf()

        schoolsViewModel = SchoolsViewModel(schoolsRepository)

        val expected = "school/$SCHOOL_ID"
        val actual = schoolsViewModel.destinationSchoolRoute(SCHOOL_ID)
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `destinationSchoolRoute failure`() = runTest {
        coEvery { schoolsRepository.getSchools() } returns listOf()

        schoolsViewModel = SchoolsViewModel(schoolsRepository)

        val expected = "schools/$SCHOOL_ID"
        val actual = schoolsViewModel.destinationSchoolRoute(SCHOOL_ID)
        assertThat(actual).isNotEqualTo(expected)
    }
}