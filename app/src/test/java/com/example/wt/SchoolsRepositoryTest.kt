package com.example.wt

import com.example.wt.model.SchoolModel
import com.example.wt.repository.SchoolsRepository
import com.example.wt.repository.network.SchoolsAPI
import com.example.wt.repository.room.RoomRepository
import com.google.common.truth.Truth.assertThat
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class SchoolsRepositoryTest {
    @MockK
    private lateinit var schoolsAPI: SchoolsAPI

    @RelaxedMockK
    private lateinit var roomRepository: RoomRepository

    @MockK
    private lateinit var schoolsRepository: SchoolsRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(TestCoroutineDispatcher())
    }

    @After
    fun tearDown() {
        clearAllMocks()
        Dispatchers.resetMain()
    }

    @Test
    fun `getSchools success empty lists`() = runTest {
        coEvery { roomRepository.getSchools() } returns listOf()
        coEvery { schoolsAPI.getSchools() } returns listOf()
        coEvery { schoolsAPI.getSchoolsSATs() } returns listOf()

        schoolsRepository = SchoolsRepository(schoolsAPI, roomRepository)

        val schools = schoolsRepository.getSchools()
        assertThat(schools?.size).isEqualTo(0)
    }

    @Test
    fun `getSchools success empty sat list`() = runTest {
        val mainList = mainList()
        coEvery { roomRepository.getSchools() } returns listOf()
        coEvery { schoolsAPI.getSchools() } returns mainList
        coEvery { schoolsAPI.getSchoolsSATs() } returns listOf()

        schoolsRepository = SchoolsRepository(schoolsAPI, roomRepository)

        val schools = schoolsRepository.getSchools()
        assertThat(schools?.size).isEqualTo(mainList.size)
    }

    @Test
    fun `getSchools success empty main list`() = runTest {
        val satList = satList()

        coEvery { roomRepository.getSchools() } returns listOf()
        coEvery { schoolsAPI.getSchools() } returns listOf()
        coEvery { schoolsAPI.getSchoolsSATs() } returns satList

        schoolsRepository = SchoolsRepository(schoolsAPI, roomRepository)

        val schools = schoolsRepository.getSchools()
        assertThat(schools?.size).isEqualTo(0)
    }

    @Test
    fun `getSchools success sat and main populated`() = runTest {
        val mainList = mainList()
        val satList = satList()

        coEvery { roomRepository.getSchools() } returns listOf()
        coEvery { schoolsAPI.getSchools() } returns mainList
        coEvery { schoolsAPI.getSchoolsSATs() } returns satList
        schoolsRepository = SchoolsRepository(schoolsAPI, roomRepository)

        val schools = schoolsRepository.getSchools()
        assertThat(schools?.size).isEqualTo(mainList.size)
    }

    private fun mainList(): List<SchoolModel> {
        val school1 = mockk<SchoolModel>(relaxed = true)
        every { school1.id } returns "1"
        val school2 = mockk<SchoolModel>(relaxed = true)
        every { school2.id } returns "2"
        val school3 = mockk<SchoolModel>(relaxed = true)
        every { school3.id } returns "3"
        return listOf(school1, school2, school3)
    }

    private fun satList(): List<SchoolModel> {
        val school1 = mockk<SchoolModel>(relaxed = true)
        every { school1.id } returns "1"
        val school2 = mockk<SchoolModel>(relaxed = true)
        every { school2.id } returns "4"
        val school3 = mockk<SchoolModel>(relaxed = true)
        every { school3.id } returns "5"
        val school4 = mockk<SchoolModel>(relaxed = true)
        every { school4.id } returns "6"
        return listOf(school1, school2, school3, school4)
    }
}