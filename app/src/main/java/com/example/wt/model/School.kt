package com.example.wt.model

interface School {
    val id: String
    val name: String?
    var satReading: String?
    var satMath: String?
    var satWriting: String?

    fun hasSATs(): Boolean
}