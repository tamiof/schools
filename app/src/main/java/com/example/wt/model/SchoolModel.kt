package com.example.wt.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "schools_table")
data class SchoolModel(
    @PrimaryKey
    @SerializedName("dbn")
    override val id: String,
    @SerializedName("school_name")
    override val name: String?,
    @SerializedName("sat_critical_reading_avg_score")
    override var satReading: String?,
    @SerializedName("sat_math_avg_score")
    override var satMath: String?,
    @SerializedName("sat_writing_avg_score")
    override var satWriting: String?
): School {
    override fun hasSATs() =
        satReading?.isNotEmpty() ?: false
                || satMath?.isNotEmpty() ?: false
                || satWriting?.isNotEmpty() ?: false
}