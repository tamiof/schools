package com.example.wt

import android.app.Application
import com.example.wt.koin.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class SchoolsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@SchoolsApplication)
            modules(listOf(
                viewModelModule,
                networkModule,
                globalModule,
                roomModule
            ))
        }
    }
}