package com.example.wt.utilities.ui

import androidx.compose.ui.graphics.Color

val DetailedViewSchoolColor = Color(105, 165, 131)
val ListSchoolColor = Color(204, 229, 255)