package com.example.wt.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.wt.model.School
import com.example.wt.repository.SchoolsRepository
import kotlinx.coroutines.flow.*

private const val SCHOOL_ID = "schoolId"

class SchoolsViewModel(private val schoolsRepository: SchoolsRepository) : ViewModel() {
    private val _schoolsState = MutableStateFlow<RequestState>(RequestState.Idle)
    val schoolsState: StateFlow<RequestState>
        get() = _schoolsState

    private val _schoolState = MutableStateFlow<SchoolRequestState>(SchoolRequestState.Idle)
    val schoolState: StateFlow<SchoolRequestState>
        get() = _schoolState

    init {
        getSchools()
    }

    private fun getSchools() = flow<RequestState> {
        _schoolsState.emit(RequestState.Loading)
        when (val result = schoolsRepository.getSchools()) {
            null -> _schoolsState.emit(RequestState.Error("An error occurred while loading schools"))
            else -> _schoolsState.emit(RequestState.Success(schools = result))
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.Eagerly,
        initialValue = RequestState.Idle
    )

    fun getSchool(id: String) = flow<SchoolRequestState> {
        _schoolState.emit(SchoolRequestState.Loading)
        when (val school = schoolsRepository.getSchool(id)) {
            null -> _schoolState.emit(SchoolRequestState.Error("An error occurred while loading schools"))
            else -> _schoolState.emit(SchoolRequestState.Success(school))
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.Eagerly,
        initialValue = SchoolRequestState.Idle
    )

    fun destinationSchoolRoute(schoolId: String) = Screen.SchoolScreen.route.replace("{$SCHOOL_ID}", schoolId)
}

sealed class RequestState {
    object Idle : RequestState()
    object Loading : RequestState()
    data class Success(val schools: List<School>) : RequestState()
    data class Error(val message: String) : RequestState()
}

sealed class SchoolRequestState {
    object Idle : SchoolRequestState()
    object Loading : SchoolRequestState()
    data class Success(val school: School) : SchoolRequestState()
    data class Error(val message: String) : SchoolRequestState()
}

sealed class Screen(val route:String){
    object SchoolsScreen : Screen("schools")
    object SchoolScreen : Screen("school/{schoolId}")
}