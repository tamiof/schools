package com.example.wt.koin

import android.app.Application
import com.example.wt.repository.room.RoomRepository
import com.example.wt.repository.room.RoomSchoolsDatabase
import org.koin.dsl.module

val roomModule = module {
    single { provideSchoolsDBRepository(get()) }
    single { provideRoom(get()) }
}

fun provideSchoolsDBRepository(roomSchoolsDatabase: RoomSchoolsDatabase) : RoomRepository {
    return RoomRepository(roomSchoolsDatabase)
}

fun provideRoom(application: Application) : RoomSchoolsDatabase? {
    return RoomSchoolsDatabase.getInstance(application)
}