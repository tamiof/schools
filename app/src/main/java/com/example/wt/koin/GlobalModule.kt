package com.example.wt.koin

import com.example.wt.repository.SchoolsRepository
import org.koin.dsl.module

val globalModule = module {
    single { SchoolsRepository(get(), get()) }
}