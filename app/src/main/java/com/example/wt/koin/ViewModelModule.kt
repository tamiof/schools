package com.example.wt.koin

import com.example.wt.viewmodel.SchoolsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { SchoolsViewModel(get()) }
}
