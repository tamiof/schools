package com.example.wt.koin

import com.example.wt.repository.network.SchoolsAPI
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideGson() }
    factory { provideOkHttpClient() }
    single { provideSchoolsAPI(get()) }
    factory { provideRetrofit(get()) }
}
fun provideGson() : Gson = GsonBuilder().create()

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl("https://dummy.com/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .client(okHttpClient)
        .build()

fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()

fun provideSchoolsAPI(retrofit: Retrofit): SchoolsAPI = retrofit.create(SchoolsAPI::class.java)

