package com.example.wt.repository

import com.example.wt.model.School
import com.example.wt.model.SchoolModel
import com.example.wt.repository.network.SchoolsAPI
import com.example.wt.repository.room.RoomRepository

class SchoolsRepository(private val schoolsAPI: SchoolsAPI, private val roomRepository: RoomRepository) {
    suspend fun getSchools(): List<School>? {
        val schools = roomRepository.getSchools()
        return when (schools.isNullOrEmpty()) {
            true -> {
                val retrievedSchools = retrieveSchools() ?: return null
                roomRepository.saveSchools(retrievedSchools)
                retrievedSchools
            }
            false -> schools
        }
    }

    private suspend fun retrieveSchools(): List<SchoolModel>? {
        val satSchoolsFromAPI = schoolsAPI.getSchoolsSATs() ?: return null
        val schoolsFromAPI = schoolsAPI.getSchools() ?: return null

        val adjustedSchools = mutableListOf<SchoolModel>()

        schoolsFromAPI.forEach { school ->
            for (i in satSchoolsFromAPI.indices) {
                val satSchool = satSchoolsFromAPI[i]
                if (satSchool.id == school.id) {
                    school.satReading = satSchool.satReading
                    school.satMath = satSchool.satMath
                    school.satWriting = satSchool.satWriting
                    break
                }
            }

            adjustedSchools.add(school)
        }

        return adjustedSchools
    }

    suspend fun getSchool(id: String) = roomRepository.getSchool(id)
}