package com.example.wt.repository.room

import com.example.wt.model.SchoolModel

class RoomRepository(roomSchoolsDatabase: RoomSchoolsDatabase) {
    private val schoolsDao: SchoolsDao? = roomSchoolsDatabase.schoolsDao()

    suspend fun saveSchools(schools : List<SchoolModel>) = schoolsDao?.insertSchools(schools)

    suspend fun getSchools() = schoolsDao?.getAllSchools()

    suspend fun getSchool(id: String) = schoolsDao?.getSchool(id)
}