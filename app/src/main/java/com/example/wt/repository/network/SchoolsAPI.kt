package com.example.wt.repository.network

import com.example.wt.model.SchoolModel
import retrofit2.http.GET

interface SchoolsAPI {
    @GET("https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
    suspend fun getSchools(): List<SchoolModel>?

    @GET("https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
    suspend fun getSchoolsSATs(): List<SchoolModel>?
}
