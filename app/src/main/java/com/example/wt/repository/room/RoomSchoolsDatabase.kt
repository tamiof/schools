package com.example.wt.repository.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.wt.model.SchoolModel

@Database(entities = [SchoolModel::class], version = 1)
abstract class RoomSchoolsDatabase : RoomDatabase() {
    abstract fun schoolsDao(): SchoolsDao?

    companion object {
        private var instance: RoomSchoolsDatabase? = null
        @Synchronized
        fun getInstance(context: Context): RoomSchoolsDatabase? {
            try {
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        RoomSchoolsDatabase::class.java, "schools_database"
                    ).build()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return instance
        }
    }
}