package com.example.wt.repository.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.wt.model.SchoolModel

@Dao
interface SchoolsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchools(schools: List<SchoolModel>?)

    @Query("SELECT * FROM schools_table")
    suspend fun getAllSchools(): List<SchoolModel>?

    @Query("SELECT * FROM schools_table WHERE id = :id")
    suspend fun getSchool(id: String): SchoolModel?
}