package com.example.wt.ui.composables.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.wt.model.School
import com.example.wt.ui.composables.navigation.screens.DetailedSchool
import com.example.wt.ui.composables.navigation.screens.SchoolsList
import com.example.wt.viewmodel.SchoolsViewModel
import com.example.wt.viewmodel.Screen

@Composable
fun SchoolsNavigationController(schoolsViewModel: SchoolsViewModel, schools: List<School>) {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screen.SchoolsScreen.route
    ) {
        composable(Screen.SchoolsScreen.route) {
            SchoolsList(schools) { schoolId ->
                schoolsViewModel.getSchool(schoolId)
                navController.navigate(schoolsViewModel.destinationSchoolRoute(schoolId))
            }
        }
        composable(route = Screen.SchoolScreen.route) {
            val state = schoolsViewModel.schoolState.collectAsState().value
            DetailedSchool(state)
        }
    }
}