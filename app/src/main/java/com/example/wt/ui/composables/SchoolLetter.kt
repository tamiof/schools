package com.example.wt.ui.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import com.example.wt.model.School
import com.example.wt.utilities.ui.dpToSp

@Composable
fun SchoolLetter(school: School, modifier: Modifier, fontSize: Dp, ) {
    Column(
        modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val text = school.name?.first() ?: "?"
        Text(text = text.toString(), fontSize = dpToSp(dp = fontSize))
    }
}