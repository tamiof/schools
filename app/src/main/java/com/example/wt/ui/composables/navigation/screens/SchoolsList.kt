package com.example.wt.ui.composables.navigation.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import com.example.wt.R
import com.example.wt.model.School
import com.example.wt.ui.composables.SchoolLetter
import com.example.wt.ui.composables.SchoolName
import com.example.wt.utilities.ui.ListSchoolColor

@Composable
fun SchoolsList(schools: List<School>, schoolClick: (String) -> Unit) {
    LazyColumn(verticalArrangement = Arrangement.spacedBy(dimensionResource(id = R.dimen.school_row_spacer_height))) {
        items(schools) { school ->
            SchoolRow(school, schoolClick)
        }
    }
}

@Composable
fun SchoolRow(school: School, schoolClick: (String) -> Unit) {
    Card(modifier = Modifier
        .fillMaxWidth()
        .height(dimensionResource(R.dimen.school_row_height))
        .padding(dimensionResource(R.dimen.school_row_padding))
        .border(dimensionResource(R.dimen.school_row_image_border_width), Color.LightGray)
        .clickable { if (school.hasSATs()) schoolClick(school.id) }) {
        Row(
            Modifier
                .fillMaxSize()
                .padding(dimensionResource(R.dimen.school_row_padding)),
            verticalAlignment = Alignment.CenterVertically
        ) {
            val letterSize = dimensionResource(R.dimen.school_row_image_width_height)
            val fontSize = dimensionResource(R.dimen.school_row_image_font_size)
            val rowLetterModifier = Modifier
                .size(letterSize)
                .clip(CircleShape)
                .background(if (school.hasSATs()) ListSchoolColor else Color.Gray)
            SchoolLetter(school, rowLetterModifier, fontSize)
            SchoolName(
                school,
                Modifier.padding(start = dimensionResource(R.dimen.school_row_name_padding))
            )
        }
    }
}