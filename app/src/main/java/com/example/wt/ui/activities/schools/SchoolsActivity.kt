package com.example.wt.ui.activities.schools

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.collectAsState
import com.example.wt.ui.composables.navigation.SchoolsNavigationController
import com.example.wt.ui.composables.uistates.ErrorUIState
import com.example.wt.ui.composables.uistates.IdleUIState
import com.example.wt.ui.composables.uistates.LoaderUIState
import com.example.wt.viewmodel.RequestState
import com.example.wt.viewmodel.SchoolsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SchoolsActivity : AppCompatActivity() {
    private val schoolsViewModel: SchoolsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            when (val schoolsState = schoolsViewModel.schoolsState.collectAsState().value) {
                is RequestState.Idle -> IdleUIState()
                is RequestState.Loading -> LoaderUIState()
                is RequestState.Error -> ErrorUIState(schoolsState.message)
                is RequestState.Success -> SchoolsNavigationController(
                    schoolsViewModel,
                    schoolsState.schools
                )
            }
        }
    }
}