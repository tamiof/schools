package com.example.wt.ui.composables.navigation.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import com.example.wt.R
import com.example.wt.model.School
import com.example.wt.ui.composables.SchoolLetter
import com.example.wt.ui.composables.SchoolName
import com.example.wt.ui.composables.uistates.ErrorUIState
import com.example.wt.ui.composables.uistates.LoaderUIState
import com.example.wt.utilities.ui.DetailedViewSchoolColor
import com.example.wt.viewmodel.SchoolRequestState

@Composable
fun DetailedSchool(state: SchoolRequestState) {
    when (state) {
        is SchoolRequestState.Idle -> {}
        is SchoolRequestState.Success -> School(state.school)
        is SchoolRequestState.Loading -> LoaderUIState()
        is SchoolRequestState.Error -> ErrorUIState(message = state.message)
    }
}

@Composable
fun School(school: School) {
    val letterSize = dimensionResource(R.dimen.school_detailed_image_width_height)
    val fontSize = dimensionResource(R.dimen.school_detailed_font_size)
    val schoolLetterModifier = Modifier
        .size(letterSize)
        .clip(CircleShape)
        .background(DetailedViewSchoolColor)
    Column(
        Modifier
            .fillMaxSize()
            .padding(top = dimensionResource(R.dimen.school_detailed_top_padding)),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        SchoolLetter(school, schoolLetterModifier, fontSize)
        SchoolName(
            school,
            Modifier.padding(top = dimensionResource(R.dimen.school_row_name_padding))
        )

        LocalContext.current.apply {
            Text("${getString(R.string.sat_reading)}: ${school.satReading}")
            Text("${getString(R.string.sat_math)}: ${school.satMath}")
            Text("${getString(R.string.sat_writing)}: ${school.satWriting}")
        }
    }
}