package com.example.wt.ui.composables

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.example.wt.R
import com.example.wt.model.School

@Composable
fun SchoolName(school: School, modifier: Modifier) {
    Text(
        text = school.name ?: LocalContext.current.getString(R.string.school_name_missing),
        modifier = modifier
    )
}